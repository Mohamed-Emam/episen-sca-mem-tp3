package com.episen.ing3.sca.tp3.backend.model;


import java.io.Serializable;


public class Order implements Serializable {
    private String id;
    private String name;
    private Long salary;


    public Order(String name) {
        this.name = name;
    }

    public Order(String id, String name, Long salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Long getSalary() {
        return salary;
    }
    public void setSalary(Long salary) {
        this.salary = salary;
    }
}