package com.episen.ing3.sca.tp3.backend.controller;

import com.episen.ing3.sca.tp3.backend.model.Order;
import com.episen.ing3.sca.tp3.backend.service.TextFileGenerator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class OrderController {

    final static Logger logger = Logger.getLogger(OrderController.class);


    @Autowired
    private TextFileGenerator textFileGenerator;

    @CrossOrigin("*")
    @PostMapping("/add/order")
    public void addOrder(@RequestBody Order order) {
        textFileGenerator.add_order(order.getName());
        logger.info("L'order insere est "+order.getName());
    }
}
