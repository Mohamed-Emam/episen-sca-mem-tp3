package com.episen.ing3.sca.tp3.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScaBackEndApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScaBackEndApplication.class, args);
    }

}
