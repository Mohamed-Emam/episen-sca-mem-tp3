import { Component } from '@angular/core';
import {OrderDTO} from "./dto/OrderDTO";
import {ActivatedRoute, Router} from "@angular/router";
import {OrderService} from "./service/OrderService";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  orderDTO: OrderDTO = new OrderDTO();

  constructor(private orderService: OrderService) { }

  ngOnInit() {
  }

  insertOrder() {
      this.orderService.add_order(this.orderDTO).subscribe(data => {
        this.orderDTO = new OrderDTO();
      });
  }
}
