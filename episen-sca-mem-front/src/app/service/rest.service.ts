import { HttpClient, HttpHeaders } from '@angular/common/http';

export abstract class RestService {

  protected completeBackendServerUrl: string = "";
  protected headers = new HttpHeaders({'content-type': 'application/json'});

  constructor(protected endpoint: string, protected http: HttpClient) {
    //this.completeBackendServerUrl = `http://172.31.249.33:8080`;
   this.completeBackendServerUrl = `http://localhost:8080`;

  }

}
